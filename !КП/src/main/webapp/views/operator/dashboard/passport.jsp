<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Monitoring</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <div id="verticalBackground">
            <div id="verticalMenu">
                <ul>
                    <li><a id="index" href="/operator/dashboard/index"><span>...</span></a></li>        
                    <li><a id="passport" href="/operator/dashboard/passport" class="current"><span>Пасспорт</span></a></li>
                    <li><a id="snils" href="/operator/dashboard/snils"><span>Снилс</span></a></li>	
                </ul>
            </div>
        </div>
        <jsp:include flush="true" page="/views/menu/top.jsp"/>  	
        <div id="mainDiv" >
            <table class="tariff_places">
                <tbody>
					<tr>
						<td class="mainColumn">ЗАГРУЖЕННЫЙ СКАН</td>
						<td><img src="data:image/jpeg;base64,${image}" alt="паспорт пользователя" width="100%" height="100%"/></td>
					</tr>
                </tbody>
            </table>
			<br/>
			<h3 style="color:red">${error}</h3>
			<br/>
			<form:form method="POST" modelAttribute="passport" action="/operator/dashboard/passport/update?${_csrf.parameterName}=${_csrf.token}">
			<form:hidden path="id"/>
			<table class="tariff_places">
                <tbody>
					<tr>
						<td class="mainColumn">СЕРИЯ</td>
						<td><form:input path="seria" cssClass="required" required="required" /></td>
					</tr>
					<tr>
						<td class="mainColumn">НОМЕР</td>
						<td><form:input path="number" cssClass="required" required="required" /></td>
					</tr>
					<tr>
						<td class="mainColumn">ОТДЕЛЕНИЕ</td>
						<td><form:input path="department" cssClass="required" required="required" /></td>
					</tr>
					<tr>
						<td class="mainColumn">ДАТА ВЫДАЧИ</td>
						<td><form:input path="dateOfIssue" cssClass="required" required="required" /></td>
					</tr>
					<tr>
						<td class="mainColumn">КОД</td>
						<td><form:input path="code" cssClass="required" required="required" /></td>
					</tr>
					<tr>
						<td class="mainColumn">ФАМИЛИЯ</td>
						<td><form:input path="secondname" cssClass="required" required="required" /></td>
					</tr>
					<tr>
						<td class="mainColumn">ИМЯ</td>
						<td><form:input path="firstname" cssClass="required" required="required" /></td>
					</tr>
					<tr>
						<td class="mainColumn">ОТЧЕСТВО</td>
						<td><form:input path="thirdname" cssClass="required" required="required" /></td>
					</tr>
					<tr>
						<td class="mainColumn">ПОЛ</td>
						<td><form:input path="sex" cssClass="required" required="required" /></td>
					</tr>
					<tr>
						<td class="mainColumn">ДАТА РОЖДЕНИЯ</td>
						<td><form:input path="birthdate" cssClass="required" required="required" /></td>
					</tr>
					<tr>
						<td class="mainColumn">МЕСТО РОЖДЕНИЯ</td>
						<td><form:input path="birthplace" cssClass="required" required="required" /></td>
					</tr>
					<tr>
						<td class="mainColumn">ИДЕНТИФИКАЦИОННАЯ СТРОКА</td>
						<td><form:input path="identify_string" cssClass="required" required="required" /></td>
					</tr>
					<tr>
						<td class="mainColumn">СТАТУС</td>
						<td><form:select path="status" >  <form:options items="${statuses}" /> </form:select></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="Сохранить" /></td>
					</tr>
                </tbody>
            </table>
			</form:form>
        </div>
    </body>
</html>
