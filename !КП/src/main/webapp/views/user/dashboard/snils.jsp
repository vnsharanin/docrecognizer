<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Monitoring</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <div id="verticalBackground">
            <div id="verticalMenu">
                <ul>
                    <li><a id="index" href="/user/dashboard/index"><span>...</span></a></li>        
                    <li><a id="passport" href="/user/dashboard/passport" ><span>Пасспорт</span></a></li>
                    <li><a id="snils" href="/user/dashboard/snils" class="current"><span>Снилс</span></a></li>	
                </ul>
            </div>
        </div>
        <jsp:include flush="true" page="/views/menu/top.jsp"/>  	
        <div id="mainDiv" >
			<form method="POST" 
				  enctype="multipart/form-data" 
				  action="/user/dashboard/snils/upload?${_csrf.parameterName}=${_csrf.token}">
				  <input type="hidden" name="MAX_FILE_SIZE" value="30000" />
				  <input type="hidden" 
						 name="${_csrf.parameterName}" 
						 value="${_csrf.token}" />
				<input type="file" name="file" />
				<input type="submit" value="Загрузить" />
			</form>
			<br/>
			<h3 style="color:red">${error}</h3>
			<br/>
            <table class="tariff_places">
                <tbody>
					<tr>
						<td class="mainColumn">ЗАГРУЖЕННЫЙ СКАН</td>
						<td><img src="data:image/jpeg;base64,${image}" alt="ваш снилс" width="100%" height="100%"/></td>
					</tr>
					<tr>
						<td class="mainColumn">СТАТУС</td>
						<td>${snils.status}</td>
					</tr>
                </tbody>
            </table>
			<br>
			<table class="tariff_places">
                <tbody>
					<tr>
						<td class="mainColumn">НОМЕР</td>
						<td>${snils.number}</td>
					</tr>
					<tr>
						<td class="mainColumn">ФАМИЛИЯ</td>
						<td>${snils.secondname}</td>
					</tr>
					<tr>
						<td class="mainColumn">ИМЯ</td>
						<td>${snils.firstname}</td>
					</tr>
					<tr>
						<td class="mainColumn">ОТЧЕСТВО</td>
						<td>${snils.thirdname}</td>
					</tr>
					<tr>
						<td class="mainColumn">ДАТА РОЖДЕНИЯ</td>
						<td>${snils.birthdate}</td>
					</tr>
					<tr>
						<td class="mainColumn">МЕСТО РОЖДЕНИЯ</td>
						<td>${snils.birthplace}</td>
					</tr>
					<tr>
						<td class="mainColumn">ПОЛ</td>
						<td>${snils.sex}</td>
					</tr>
					<tr>
						<td class="mainColumn">ДАТА РЕГИСТРАЦИИ</td>
						<td>${snils.registerdate}</td>
					</tr>
                </tbody>
            </table>	
        </div>
    </body>
</html>
