<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>	
        <link rel="stylesheet" href="/resources/css/style-mymodal.css"> <!-- Gem style -->
        <script src="/resources/js/modernizr.js"></script> <!-- Modernizr -->
        <script src="/resources/js/main.js"></script> <!-- Gem jQuery -->

        <link href="/resources/css/style-vertical.css" rel="stylesheet" media="all" />
        <link href="/resources/css/style-buttons.css" rel="stylesheet" media="all" />
        <link href="/resources/css/style-mainDiv.css" rel="stylesheet" media="all" />
        <link href="/resources/css/style.css" rel="stylesheet" media="all" />
        <link href="/resources/css/style-align.css" rel="stylesheet" media="all" />
        <link href="/resources/css/style-modal.css" rel="stylesheet" media="all" />
        <link href="/resources/css/style-tables.css" rel="stylesheet" media="all" />
		<script type="text/javascript">
			function load(){
				$('#${active}').attr('class', 'current');
				$('#${vertical_menu}').attr('class', 'current');
				//document.getElementById('${vertical_menu}').className='current'
			}
		</script>
    </head>
    <body onload="javascript:load()">
        <div id="menu">
            <ul>
		<sec:authorize access="hasRole('USER')">
                    <li><a id="dashboard" href="/user/dashboard/index"><span>Рабочий стол</span></a></li>	
                    <li>
                        <p><a class="accountLogOut" href="<c:url value="/logout" />"></a></p>
                    </li>
		</sec:authorize>
		<sec:authorize access="hasRole('ADMIN')">	
                    <li><a id="dashboard" href="/admin/dashboard/index"><span>Рабочий стол</span></a></li>	
                    <li><p><a class="accountLogOut" href="<c:url value="/logout" />"></a></p></li>
		</sec:authorize>
		<sec:authorize access="hasRole('OPERATOR')">
                     <li><a id="dashboard" href="/operator/dashboard/index"><span>Рабочий стол</span></a></li>
                    <li><p><a class="accountLogOut" href="<c:url value="/logout" />"></a></p></li>
		</sec:authorize>				
		<sec:authorize access="!isAuthenticated()">
					<li><a id="home" href="/visitor/home"><span>Главная</span></a></li>	
                    <li><a href="#0" class="accountLogIn"><span>Вход/Регистрация</span></a></li>				 
		</sec:authorize>
            </ul>	
        </div>
        <jsp:include flush="true" page="authorization.jsp"/>
    </body>
</html>
