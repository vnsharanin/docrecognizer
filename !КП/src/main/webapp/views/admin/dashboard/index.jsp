<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Monitoring</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	   <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
	   <script type="text/javascript">
		 google.charts.load('current', {packages: ['corechart']});
	   </script>
	   <link rel="stylesheet" type="text/css" href="/resources/js/calendar/tcal.css" />
	   <script type="text/javascript" src="/resources/js/calendar/tcal.js"></script> 
	
    </head>
    <body>
        <div id="verticalBackground">
            <div id="verticalMenu">
                <ul>
                    <li><a id="index" href="/admin/dashboard/index" class="current"><span>...</span></a></li>
					<li><a id="index" href="/admin/dashboard/statistics" ><span>Производительность</span></a></li>
                </ul>
            </div>
        </div>
        <jsp:include flush="true" page="/views/menu/top.jsp"/>  	
        <div id="mainDiv" >
        
            <H2>СООТНОШЕНИЯ</H2><br/>
			Дата: <input type="text" id="date" name="date" class="tcal" value="" />
			<input type="button" value="Загрузить" onclick="dateChange()" />
			<br/>
			<div id="container" style="width: 550px; height: 400px; margin: 0 auto"></div>
        </div>
		<script type="text/javascript">
			function updateDataOnDate(url) {
				$.ajax({
					//dataType: "json",
					method: "get",
					url: url,
					success: function(data) {
								var obj = JSON.parse(data);
							    DrawChart(obj.operatorsAndPassports, obj.systemAndPassports, obj.operatorsAndSnilses, obj.systemAndSnilses);//new chart
							},
					error: function( xhr, textStatus ,errorThrown ) {
								alert('Err: ' + [ xhr.status, textStatus ] +' '+ errorThrown);
							}
				});
			}
			function dateChange() {
				var value = $('#date').val();
				updateDataOnDate('/admin/dashboard/index/OnDate?date=' + value);
			}
		
			function DrawChart (operatorsAndPassports, systemAndPassports, operatorsAndSnilses, systemAndSnilses) {
				$('#container').empty();//crear div
				if (operatorsAndPassports == null)
					operatorsAndPassports = ${operatorsAndPassports}
				if (systemAndPassports == null)
					systemAndPassports = ${systemAndPassports}
				if (operatorsAndSnilses == null)
					operatorsAndSnilses = ${operatorsAndSnilses}
				if (systemAndSnilses == null)
					systemAndSnilses = ${systemAndSnilses}
				
				// Instantiate and draw the chart.
				var chart = new google.visualization.PieChart(document.getElementById('container'));
				chart.draw(data, options);
				// Set chart options
				var options = {'title':'Operators and system procent',
				   'width':550,
				   'height':400};
				   // Define the chart to be drawn.
				var data = new google.visualization.DataTable();
				data.addColumn('string', 'Process type');
				data.addColumn('number', 'Percentage');
				data.addRows([
				   ['Operators (snilses)', operatorsAndSnilses],
				   ['Operators (passports)', operatorsAndPassports],
				   ['System (passports)', systemAndPassports],
				   ['System (snilses)', systemAndSnilses]
				]);
				// Instantiate and draw the chart.
				var chart = new google.visualization.PieChart(document.getElementById('container'));
				chart.draw(data, options);
			}
			google.charts.setOnLoadCallback (DrawChart);
		</script>	
    </body>
</html>
