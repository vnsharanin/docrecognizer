<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Monitoring</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <div id="verticalBackground">
            <div id="verticalMenu">
                <ul>
                    <li><a id="index" href="/admin/dashboard/index" ><span>...</span></a></li>
					<li><a id="index" href="/admin/dashboard/statistics" class="current"><span>Производительность</span></a></li>
                </ul>
            </div>
        </div>
        <jsp:include flush="true" page="/views/menu/top.jsp"/>  	
        <div id="mainDiv" >
            <H2>МЕСЯЧНЫЙ КОЕФФИЦИЕНТ СОТРУДНИКОВ</H2><br/>
			<H5>(отличный показатель = 1)</H5><br/>
            <table class="services">
                <thead>
                <th>Оператор</th>
				<th>Коэффициент обработки снилсов</th>
                <th>Коэффициент обработки паспортов</th>
				<th>Средний коэффииент</th>
				<th>Месяц</th>   				
                </thead>
                <tbody>
					 <c:forEach var = "item" items = "${coefficients}">
                        <tr>
                            <td>${item.name}</td>
							<td>${item.snilses_coeff}</td>
                            <td>${item.passports_coeff}</td>
                            <td>${item.avg}</td>
							<td>${item.mounth}</td>
                        </tr>
					</c:forEach>
                </tbody>
            </table>	
        </div>
    </body>
</html>
