﻿drop database clientdb;
create database clientdb;
use clientdb;	
	
CREATE TABLE persistent_logins (
    username NVARCHAR(64) NOT NULL,
    series NVARCHAR(64) NOT NULL,
    token NVARCHAR(64) NOT NULL,
    last_used TIMESTAMP NOT NULL,
    PRIMARY KEY (series)
);

create table Users (
   id BIGINT NOT NULL AUTO_INCREMENT,
   telephone NVARCHAR(20) NOT NULL,

   password NVARCHAR(256) NOT NULL,
   status NVARCHAR(30) NOT NULL,
   PRIMARY KEY (id),
   UNIQUE (telephone)
);

create table Roles(
   id BIGINT NOT NULL AUTO_INCREMENT,
   name NVARCHAR(30) NOT NULL,
   PRIMARY KEY (id),
   UNIQUE (name)
);
  
CREATE TABLE Users_Roles (
	id BIGINT NOT NULL AUTO_INCREMENT,
    user BIGINT NOT NULL,
    role BIGINT NOT NULL,
    PRIMARY KEY (id),
	UNIQUE (user, role),
    CONSTRAINT FK_Users_Roles_Users FOREIGN KEY (user) REFERENCES Users (id),
    CONSTRAINT FK_Users_Roles_Roles FOREIGN KEY (role) REFERENCES Roles (id)
);

create table Passports (
   id BIGINT NOT NULL AUTO_INCREMENT,
   user BIGINT NOT NULL,

   seria NVARCHAR(20) NULL,
   number NVARCHAR(256) NULL,
   department NVARCHAR(256) NULL,
   date_of_issue NVARCHAR(256) NULL,
   code NVARCHAR(256) NULL,
   secondname NVARCHAR(256) NULL,
   firstname NVARCHAR(256) NULL,
   thirdname NVARCHAR(256) NULL,
   sex NVARCHAR(4) NULL,
   birthdate NVARCHAR(256) NULL,
   birthplace NVARCHAR(256) NULL,
   identify_string NVARCHAR(256) NULL,

   path_to_original NVARCHAR(256) NOT NULL,
   load_date DATE NOT NULL,
   status NVARCHAR(30) NOT NULL,
   PRIMARY KEY (id),
   CONSTRAINT FK_PassportData_Users FOREIGN KEY (user) REFERENCES Users (id)
);

CREATE TABLE Passports_Queue (
	id BIGINT NOT NULL AUTO_INCREMENT,
    user BIGINT NULL,
    passport BIGINT NOT NULL,
	start_date DATE NOT NULL,
	end_date DATE NULL,
	status NVARCHAR(30) NOT NULL,
    PRIMARY KEY (id),
	UNIQUE (user, passport),
    CONSTRAINT FK_Passports_Queue_Users FOREIGN KEY (user) REFERENCES Users (id),
    CONSTRAINT FK_Passports_Queue_Passports FOREIGN KEY (passport) REFERENCES Passports (id)
);

create table Snilses (
   id BIGINT NOT NULL AUTO_INCREMENT,
   user BIGINT NOT NULL,

   number NVARCHAR(256) NULL,
   secondname NVARCHAR(256) NULL,
   firstname NVARCHAR(256) NULL,
   thirdname NVARCHAR(256) NULL,
   birthdate NVARCHAR(256) NULL,
   birthplace NVARCHAR(256) NULL,
   sex NVARCHAR(10) NULL,
   registerdate NVARCHAR(256) NULL,

   path_to_original NVARCHAR(256) NOT NULL,
   load_date DATE NOT NULL,
   status NVARCHAR(30) NOT NULL,
   PRIMARY KEY (id),
   CONSTRAINT FK_SnilsData_Users FOREIGN KEY (user) REFERENCES Users (id)
);

CREATE TABLE Snilses_Queue (
	id BIGINT NOT NULL AUTO_INCREMENT,
    user BIGINT NULL,
    snils BIGINT NOT NULL,
	start_date DATE NOT NULL,
	end_date DATE NULL,
	status NVARCHAR(30) NOT NULL,
    PRIMARY KEY (id),
	UNIQUE (user, snils),
    CONSTRAINT FK_Snilses_Queue_Users FOREIGN KEY (user) REFERENCES Users (id),
    CONSTRAINT FK_Snilses_Queue_Snilses FOREIGN KEY (snils) REFERENCES Snilses (id)
);

 
INSERT INTO Roles(name)
VALUES ('USER'),('ADMIN'),('OPERATOR');


INSERT INTO Users(telephone, password, status)
VALUES ('+7(920)902-8546','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','Active')
, ('+7(929)028-3258','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','Active')
, ('+7(929)028-3158','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','Active')
, ('+7(929)028-3358','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','Active')
, ('+7(929)028-3558','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','Active')
, ('+7(929)028-3658','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','Active');


INSERT INTO Users_Roles (user, role)
  VALUES (1,2), (2,3), (3,1), (4,3),(5,3),(6,3);

