package com.shar.domain;

import java.util.Comparator;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

//USER ADMIN DBA MANAGER
@Entity
@Table(name = "snilses_queue")
public class SnilsQueue {

    public SnilsQueue() {
    }

    public SnilsQueue(User user, Snils snils) {
        this.user = user;
        this.snils = snils;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /*
     @Id
     @Column(name = "user_id", length = 20, unique = true, nullable = false)
     private int userId;

     @Id
     @Column(name = "role_id", length = 20, unique = true, nullable = false)
     private int roleId;*/

    @JoinColumn(name = "user")
    @ManyToOne
    private User user;

    @JoinColumn(name = "snils")
    @ManyToOne
    private Snils snils;

    @Column(name = "start_date", unique = false, nullable = false)
    private Date startDate;

    @Column(name = "end_date", unique = false, nullable = false)
    private Date endDate;

    @Column(name = "status", nullable = false)
    private String status;
    /* public UserRole(){};
     public UserRole(int userid, int roleId){
     this.userId = userId;
     this.roleId = roleId;
     }*/
    /*
     public int getUserId() {
     return userId;
     }
     public void setUserId(int userId) {
     this.userId = userId;
     }
     public int getRoleId() {
     return roleId;
     }
     public void setRoleId(int roleId) {
     this.roleId = roleId;
     }*/

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Snils getSnils() {
        return snils;
    }

    public void setSnils(Snils snils) {
        this.snils = snils;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public static Comparator<SnilsQueue> StartDateComparator = new Comparator<SnilsQueue>() {
        @Override
        public int compare(SnilsQueue sq1, SnilsQueue sq2) {
            return sq1.getStartDate().compareTo(sq2.getStartDate());
        }
    };
}
