
package com.shar.domain;

import java.util.Date;


public class OperatorCoefficient {
    private String name;
    private double passports_coeff;
    private double snilses_coeff;
    private double avg;
    private int mounth;

    public OperatorCoefficient(String name,double passports_coeff,double snilses_coeff){
        this.name = name;
        this.passports_coeff = passports_coeff;
        this.snilses_coeff = snilses_coeff;
        this.avg = (passports_coeff+snilses_coeff)/2;
        this.mounth = new Date().getMonth();
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPassports_coeff() {
        return passports_coeff;
    }

    public void setPassports_coeff(double passports_coeff) {
        this.passports_coeff = passports_coeff;
    }

    public double getSnilses_coeff() {
        return snilses_coeff;
    }

    public void setSnilses_coeff(double snilses_coeff) {
        this.snilses_coeff = snilses_coeff;
    }

    public double getAvg() {
        return avg;
    }

    public void setAvg(double avg) {
        this.avg = avg;
    }

    public int getMounth() {
        return mounth;
    }

    public void setMounth(int mounth) {
        this.mounth = mounth;
    }
}
