package com.shar.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

//USER ADMIN DBA MANAGER
@Entity
@Table(name = "users_roles")
public class UserRole {

    public UserRole(){}
    public UserRole(User user, Role role){
        this.user = user;
        this.role = role;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /*
     @Id
     @Column(name = "user_id", length = 20, unique = true, nullable = false)
     private int userId;

     @Id
     @Column(name = "role_id", length = 20, unique = true, nullable = false)
     private int roleId;*/

    @JoinColumn(name = "user")
    @ManyToOne
    private User user;

    @JoinColumn(name = "role")
    @ManyToOne
    private Role role;

    /* public UserRole(){};
     public UserRole(int userid, int roleId){
     this.userId = userId;
     this.roleId = roleId;
     }*/
    /*
     public int getUserId() {
     return userId;
     }
     public void setUserId(int userId) {
     this.userId = userId;
     }
     public int getRoleId() {
     return roleId;
     }
     public void setRoleId(int roleId) {
     this.roleId = roleId;
     }*/
    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

}
