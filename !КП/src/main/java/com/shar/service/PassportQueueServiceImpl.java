/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shar.service;

import com.shar.configuration.MyGlobal;
import com.shar.dao.PassportQueueDao;
import com.shar.domain.Passport;
import com.shar.domain.PassportQueue;
import com.shar.domain.StatusConstantClass;
import com.shar.domain.User;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("passportQueueService")
@Transactional
public class PassportQueueServiceImpl implements PassportQueueService {

    @Autowired
    private PassportQueueDao dao;

    private static final Logger logger = Logger.getLogger(PassportQueueServiceImpl.class);

    
    public Passport getFirstPassport(User operator) {
        int size = operator.getUserPassportsInQueue().size();
        if (size != 0) {
            PassportQueue[] passportsInQueue = operator.getUserPassportsInQueue().toArray(new PassportQueue[size]);
            Arrays.sort(passportsInQueue, PassportQueue.StartDateComparator);
            for (PassportQueue pq : passportsInQueue) {
                if (pq.getStatus().equals(StatusConstantClass.REVIEW)) {
                    return pq.getPassport();
                }
            }
        }
        return null;
    }
    public int getCountPassportsInQueue(User operator) {
        int size = operator.getUserPassportsInQueue().size();
        if (size != 0) {
            PassportQueue[] passportsInQueue = operator.getUserPassportsInQueue().toArray(new PassportQueue[size]);
            Arrays.sort(passportsInQueue, PassportQueue.StartDateComparator);
            for (PassportQueue pq : passportsInQueue) {
                if (pq.getStatus().equals(StatusConstantClass.REVIEW)) {
                    size++;
                }
            }
        }
        return size;
    }

    public void updatePassportQueue(User operator, Passport passport) {
        PassportQueue passportQueue = null;
        for (PassportQueue pq : operator.getUserPassportsInQueue()) {
            if (pq.getUser().getId() == operator.getId() && pq.getPassport().getId() == passport.getId()) {
                passportQueue = pq;
            }
        }
        if (passportQueue == null) {
            passportQueue.setUser(operator);
            passportQueue.setPassport(passport);
            passportQueue.setStartDate(new java.util.Date());
        }
        passportQueue.setEndDate(new java.util.Date());
        passportQueue.setStatus(StatusConstantClass.SUCCESS);
        dao.createOrUpdatePassportQueue(passportQueue);
    }
    
    public Map<String,Double> calcCoefficient(List<User> operators){
        List<PassportQueue> passportsQueue = dao.getListPassportQueue();
        Map<String,Double> result = new HashMap<String,Double>(operators.size());

        for(User operator : operators){
            int countPasports = 0;
            int countPasportsInTime = 0;
            for (PassportQueue pq : passportsQueue){
                if (pq.getUser().getId() == operator.getId()){
                    if (pq.getStartDate().getMonth() == new Date().getMonth()) {
                        countPasports++;
                        if (pq.getStartDate().equals(pq.getEndDate())) {
                            countPasportsInTime++;
                        }
                    }
                }
            }
            double coefficient = countPasports - countPasportsInTime;//(countPasportsInTime != 0) ? countPasports / countPasportsInTime : countPasports;
            result.put(operator.getTelephone(), coefficient);
        }
        return result;
    }
    public int countPassportsInQueueByDate(Date date){
        int i = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for (PassportQueue pq : dao.getListPassportQueue()){
            if (sdf.format(pq.getStartDate()).equals(sdf.format(date)))
                i++;
        }
        logger.debug("countPassportsInQueueByDate="+i+" Date:"+date);
        return i;
    }
}
