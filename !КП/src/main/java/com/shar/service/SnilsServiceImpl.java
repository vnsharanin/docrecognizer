package com.shar.service;

import com.shar.dao.SnilsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.shar.domain.Snils;
import com.shar.domain.StatusConstantClass;
import com.shar.domain.User;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

@Service("snilsService")
@Transactional
public class SnilsServiceImpl implements SnilsService {

    private static final Logger logger = Logger.getLogger(SnilsServiceImpl.class);

    @Autowired
    private SnilsDao dao;

    public List<Snils> listSnilses() {
        return dao.listSnilses();
    }

    public Snils saveSnils(byte[] bytes, User user) {
        return dao.saveSnils(bytes, user);
    }

    public Snils getLastSnils(User user) {
        //we can get it from user object, that's why we won't use DAO
        //one of statuses must be
        /*for (Snils snils : user.getUserSnilses()) {
            if (snils.getStatus().equals(StatusConstantClass.REVIEW)) {
                logger.debug(snils.toString());
                return snils;
            }
        }
        for (Snils snils : user.getUserSnilses()) {
            if (snils.getStatus().equals(StatusConstantClass.ACTIVE)) {
                logger.debug(snils.toString());
                return snils;
            }
        }
        for (Snils snils : user.getUserSnilses()) {
            if (snils.getStatus().equals(StatusConstantClass.BLOCKED)) {
                logger.debug(snils.toString());
                return snils;
            }
        }*/
        int pos = 1;
        logger.debug("Snils size="+user.getUserSnilses().size());
        for (Snils snils : user.getUserSnilses()) {
            if ((user.getUserSnilses().size())==pos) {
                logger.debug(snils.toString());
                return snils;
            }
            pos++;
        }
        logger.debug("Snils not found");
        return null;
    }

    public Snils getSnilsById(int id) {
        return dao.getSnilsById(id);
    }

    public void updateSnils(Snils snils, Snils formData) {
        //previous passports to fail
        for (Snils previousSnils : snils.getUser().getUserSnilses()) {
            if (previousSnils.getId() != snils.getId()) {
                previousSnils.setStatus(StatusConstantClass.BLOCKED);
                dao.updateSnils(previousSnils);
            }
        }
        snils.setBirthdate(formData.getBirthdate());
        snils.setBirthplace(formData.getBirthplace());
        snils.setFirstname(formData.getFirstname());
        snils.setNumber(formData.getNumber());
        snils.setRegisterdate(formData.getRegisterdate());
        snils.setSecondname(formData.getSecondname());
        snils.setSex(formData.getSex());
        snils.setStatus(formData.getStatus());
        snils.setThirdname(formData.getThirdname());
        dao.updateSnils(snils);
    }

    public int countSnilsByDate(Date date) {
        int i = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for (Snils s : dao.listSnilses()) {
            if (sdf.format(s.getLoadDate()).equals(sdf.format(date))) {
                i++;
            }
        }
        logger.debug("countSnilsByDate="+i+" Date:"+date);
        return i;
    }
}
