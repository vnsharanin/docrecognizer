package com.shar.service;

import com.shar.domain.Role;
import com.shar.domain.User;
import com.shar.domain.UserRole;
import java.util.List;

public interface UserRoleService {
    List<Role> getRolesByUser(int id);
    List<User> getUsersByRole(int id);
    List<UserRole> getUserRoleByUserId(int userId);
    //Map<User, Role> getUsersRoles();
    void saveUserRole(int roleId, int userId);
    //void removeUserRole(int roleId, int userId);    
}
