package com.shar.dao;

import com.shar.domain.Passport;
import com.shar.domain.User;
import java.util.List;

public interface PassportDao {
        List<Passport> listPassports();
	Passport savePassport(byte[] bytes, User user);
        void updatePassport(Passport passport);
        Passport getPassportById(int id);
}

