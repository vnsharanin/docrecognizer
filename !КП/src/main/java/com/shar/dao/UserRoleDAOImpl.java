package com.shar.dao;

import com.shar.domain.Role;
import com.shar.domain.User;
import com.shar.domain.UserRole;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("IUserRoleDao")
public class UserRoleDAOImpl implements UserRoleDAO {

    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    @SuppressWarnings("unchecked")
    public List<UserRole> getUserRoleByUserId(int user) {
        return sessionFactory.getCurrentSession().createCriteria(UserRole.class).add(Restrictions.eq("user.id", user)).list();  
    }

    public List<User> getUsersByRole(int id) {
        Query query = sessionFactory.getCurrentSession().createQuery("select user from UserRole where role = :id");
        query.setInteger("id", id);
        return query.list();
    }

    public List<Role> getRolesByUser(int id) {
        Query query = sessionFactory.getCurrentSession().createQuery("select role from UserRole where user = :id");
        query.setInteger("id", id);
        return query.list();
    }

    public void saveUserRole(int roleId, int userId) {

    }
}
