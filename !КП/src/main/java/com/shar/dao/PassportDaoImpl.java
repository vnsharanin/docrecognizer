package com.shar.dao;

import com.shar.configuration.MyGlobal;
import com.shar.domain.Passport;
import com.shar.domain.PassportQueue;
import com.shar.domain.StatusConstantClass;
import com.shar.domain.User;
import com.shar.recognizer.PassportData;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import org.springframework.stereotype.Repository;

import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import javax.imageio.ImageIO;
import org.hibernate.Query;

import org.apache.log4j.Logger;

@Repository("passportDao")
public class PassportDaoImpl extends AbstractDao<Integer, Passport> implements PassportDao {

    private static final Logger logger = Logger.getLogger(PassportDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;
    Transaction tx = null;

    public Passport savePassport(byte[] bytes, User user) {
        String fileName = user.getTelephone() + ".png";
        String fullPath = MyGlobal.UPLOADED_PASSPORTS_FOLDER + fileName;
        try {
            //saving original passport image
            BufferedImage resultImage = ImageIO.read(new ByteArrayInputStream(bytes));
            ImageIO.write(resultImage, "png", new File(MyGlobal.UPLOADED_PASSPORTS_FOLDER, fileName));//фотку сохранили

            //saving passport data
            Passport passport = new Passport();
            passport.setPath_to_original(fullPath);
            passport.setUser(user);
            passport.setLoadDate(new java.util.Date());
            PassportData.fillPassportData(passport, fileName);
            sessionFactory.getCurrentSession().save(passport);
            if (passport.getStatus() == StatusConstantClass.REVIEW) {
                Query query = sessionFactory.getCurrentSession().createQuery(""
                        + "select u from User u"
                        + " inner join u.userRoles ur"
                        + " inner join ur.role r"
                        + " left outer join u.userPassportsInQueue pq"
                        + " where r.name = :role and u.status = :status group by u.id order by count(pq.user)");
                query.setString("status", StatusConstantClass.ACTIVE);
                query.setString("role", "OPERATOR");
                query.setFirstResult(0);
                query.setMaxResults(1);
                User operator = (User) query.uniqueResult();
                // operator.getUserPassportsInQueue().add(new PassportQueue());
                PassportQueue passportQueue = new PassportQueue();
                passportQueue.setPassport(passport);
                passportQueue.setUser(operator);
                passportQueue.setStatus(StatusConstantClass.REVIEW);
                passportQueue.setStartDate(new java.util.Date());
                sessionFactory.getCurrentSession().save(passportQueue);
            }
            return passport;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public List<Passport> listPassports() {
        return sessionFactory.getCurrentSession().createQuery("from Passport").list();
    }

    public void updatePassport(Passport passport) {
        sessionFactory.getCurrentSession().update(passport);
    }

    public Passport getPassportById(int id) {
        Query query = sessionFactory.getCurrentSession().createQuery(""
                + "select p from Passport p"
                + " where p.id = :id");
        query.setInteger("id", id);
        query.setFirstResult(0);
        query.setMaxResults(1);
        return (Passport) query.uniqueResult();
    }
}
