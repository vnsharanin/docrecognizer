package com.shar.dao;

import com.shar.domain.User;
import java.util.List;

public interface UserDao {
        List<User> listUsers();
        List<User> listActiveOperators();
	User findById(int id);
	User saveUser(User user);
        User findByTelephone(String telephone);
}

