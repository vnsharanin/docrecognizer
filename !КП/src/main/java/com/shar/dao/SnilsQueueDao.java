/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shar.dao;

import com.shar.domain.SnilsQueue;
import java.util.List;

/**
 *
 * @author v_sh9
 */
public interface SnilsQueueDao {
      void createOrUpdateSnilsQueue(SnilsQueue snilsQueue);
      List<SnilsQueue> getListSnilsQueue();
}
