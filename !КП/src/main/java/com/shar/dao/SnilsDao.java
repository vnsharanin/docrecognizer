package com.shar.dao;

import com.shar.domain.Snils;
import com.shar.domain.User;
import java.util.List;

public interface SnilsDao {
	List<Snils> listSnilses();
	Snils saveSnils(byte[] bytes, User user);
        void updateSnils(Snils snils);
        Snils getSnilsById(int id);
}

