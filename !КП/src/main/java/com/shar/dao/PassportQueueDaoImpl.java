/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shar.dao;

import com.shar.domain.PassportQueue;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("PassportQueueDao")
public class PassportQueueDaoImpl implements PassportQueueDao {

    @Autowired
    private SessionFactory sessionFactory;

    public void createOrUpdatePassportQueue(PassportQueue passportQueue) {
        if (passportQueue.getId() > 0) {
            sessionFactory.getCurrentSession().update(passportQueue);
        } else {
            sessionFactory.getCurrentSession().save(passportQueue);
        }
    }
    public List<PassportQueue> getListPassportQueue(){
         return sessionFactory.getCurrentSession().createQuery("from PassportQueue").list();
    }
}
