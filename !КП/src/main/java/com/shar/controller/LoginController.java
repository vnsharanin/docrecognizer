package com.shar.controller;

import com.shar.domain.User;
import com.shar.service.UserRoleService;
import com.shar.service.UserService;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {

    @Autowired
    UserService userService;
    
    @RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
    public String accessDeniedPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "accessDenied";
    }

    @RequestMapping(value = {"/register"}, method = RequestMethod.POST)
  //  @ResponseBody
    public String addUser(HttpServletRequest request) {
       
        String telephone = request.getParameter("telephone").replace(" ", "");
        String password = generate(8, 12);
         if (!telephone.matches("^\\+7\\([0-9]{3}\\)[0-9]{3}-[0-9]{4}$")) { System.out.println("Incorrect number format!"); }
         else
        if(userService.saveUser(new User(telephone,password)).getStatus().equals("Active")) {
         /*   SMSCSender sd= new SMSCSender("vsharanin", "123654789s", "utf-8", true);
            sd.sendSms(telephone, "Password:"+password, 1, "", "", 0, "", "");
            sd.getSmsCost(telephone, "Вы успешно зарегистрированы!", 0, 0, "", "");  
            sd.getBalance();*/
        } //else when I make ajax - return "error message as JSON"
        
       System.out.println("PHONE: "+telephone + " PASSWORD: "+password+" ");
        
       return "redirect:/visitor/home";
    }  
  
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage() {
        return "login";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/visitor/home";
    }

    private String getPrincipal() {
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            userName = ((UserDetails) principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }
    
    //ГЕНЕРАТОР ПАРОЛЯ
    public static String generate(int from, int to) {
        String pass  = "";
        Random r     = new Random();
        int cntchars = from + r.nextInt(to - from + 1);

        for (int i = 0; i < cntchars; ++i) {
            char next = 0;
            int range = 10;

            switch(r.nextInt(3)) {
                case 0: {next = '0'; range = 10;} break;
                case 1: {next = 'a'; range = 26;} break;
                case 2: {next = 'A'; range = 26;} break;
            }

            pass += (char)((r.nextInt(range)) + next);
        }

        return pass;
    }


}
