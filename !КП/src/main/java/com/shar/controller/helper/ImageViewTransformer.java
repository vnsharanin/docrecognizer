/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shar.controller.helper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;
/**
 *
 * @author v_sh9
 */
public class ImageViewTransformer {
    public static String out(String path){
        FileInputStream fis = null;
        try {
            File file = new File(path);
            fis = new FileInputStream(file);
            ByteArrayOutputStream bos=new ByteArrayOutputStream();
            int b;
            byte[] buffer = new byte[1024];
            while((b=fis.read(buffer))!=-1){
                bos.write(buffer,0,b);
            }           byte[] fileBytes=bos.toByteArray();
            fis.close();
            bos.close();
            byte[] encoded=Base64.encodeBase64(fileBytes);
            String encodedString = new String(encoded);
            return encodedString;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ImageViewTransformer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ImageViewTransformer.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(ImageViewTransformer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
}
