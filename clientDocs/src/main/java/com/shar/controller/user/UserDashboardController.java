package com.shar.controller.user;

import com.shar.configuration.MyGlobal;
import com.shar.controller.helper.ImageViewTransformer;
import com.shar.domain.Passport;
import com.shar.domain.Snils;
import com.shar.domain.StatusConstantClass;
import com.shar.domain.User;
import com.shar.service.PassportService;
import com.shar.service.SnilsService;
import com.shar.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

@Controller
public class UserDashboardController {

    @Autowired
    UserService userService;

    @Autowired
    PassportService passportService;

    @Autowired
    SnilsService snilsService;

    @RequestMapping("/user/dashboard/index")
    public String userDashBoardMain(ModelMap model) {
        User user = userService.findByTelephone(SecurityContextHolder.getContext().getAuthentication().getName());
        Snils snils = snilsService.getLastSnils(user);
        Passport passport = passportService.getLastPassport(user);

        model.addAttribute("snilsStatus", ((snils != null) ? snils.getStatus() : "Document is not loaded"));
        model.addAttribute("passportStatus", ((passport != null) ? passport.getStatus() : "Document is not loaded"));

        model.addAttribute("active", "dashboard");
        return "user/dashboard/index";
    }

    @RequestMapping("/user/dashboard/passport")
    public String userDashBoardPassport(ModelMap model) {
        User user = userService.findByTelephone(SecurityContextHolder.getContext().getAuthentication().getName());
        Passport passport = passportService.getLastPassport(user);

        model.addAttribute("passport", passport);
        if (passport != null) {
            model.put("image", ImageViewTransformer.out(passport.getPath_to_original()));
        }

        model.addAttribute("active", "dashboard");
        return "user/dashboard/passport";
    }

    @RequestMapping("/user/dashboard/snils")
    public String userDashBoardSnils(ModelMap model) {
        User user = userService.findByTelephone(SecurityContextHolder.getContext().getAuthentication().getName());
        Snils snils = snilsService.getLastSnils(user);

        model.addAttribute("snils", snils);
        if (snils != null) {
            model.put("image", ImageViewTransformer.out(snils.getPath_to_original()));
        }

        model.addAttribute("active", "dashboard");
        return "user/dashboard/snils";
    }

    @PostMapping("/user/dashboard/passport/upload")
    //@RequestMapping(value = {"/user/dashboard/passport/upload"}, method = RequestMethod.POST)
    public String passportScanUpload(ModelMap model, @RequestParam("file") MultipartFile file) {
        User user = userService.findByTelephone(SecurityContextHolder.getContext().getAuthentication().getName());

        if (file.isEmpty()) {
            model.addAttribute("error", "File not selected");
        } else {
            try {
                //validation
                Passport passport = passportService.getLastPassport(user);
                if (passport != null && passport.getStatus().equals(StatusConstantClass.REVIEW)) {
                    model.addAttribute("error", "You cannot upload new document while previous document is on review");
                    return userDashBoardPassport(model);
                }
                
                passportService.savePassport(file.getBytes(), user);
            } catch (IOException ex) {
                Logger.getLogger(UserDashboardController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return userDashBoardPassport(model);
    }

    @PostMapping("/user/dashboard/snils/upload")
    //@RequestMapping(value = {"/user/dashboard/passport/upload"}, method = RequestMethod.POST)
    public String snilsScanUpload(ModelMap model, @RequestParam("file") MultipartFile file) {
        User user = userService.findByTelephone(SecurityContextHolder.getContext().getAuthentication().getName());

        if (file.isEmpty()) {
            model.addAttribute("error", "File not selected");
        } else {
            try {
                //validation
                Snils snils = snilsService.getLastSnils(user);
                if (snils != null && snils.getStatus().equals(StatusConstantClass.REVIEW)) {
                    model.addAttribute("error", "You cannot upload new document while previous document is on review");
                    return userDashBoardSnils(model);
                }
                
                snilsService.saveSnils(file.getBytes(), user);
            } catch (IOException ex) {
                Logger.getLogger(UserDashboardController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return userDashBoardSnils(model);
    }

    private String toJson(Object value) {
        ObjectMapper mapper = new ObjectMapper();
        String result = null;
        try {
            result = mapper.writeValueAsString(value);
        } catch (IOException ex) {
            Logger.getLogger(UserDashboardController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}
