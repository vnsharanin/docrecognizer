package com.shar.controller.operator;

import com.shar.controller.helper.ImageViewTransformer;
import com.shar.domain.Passport;
import com.shar.domain.Snils;
import com.shar.domain.StatusConstantClass;
import com.shar.domain.User;
import com.shar.service.PassportQueueService;
import com.shar.service.PassportService;
import com.shar.service.SnilsQueueService;
import com.shar.service.SnilsService;
import com.shar.service.UserService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class OperatorDashboardController {

    @Autowired
    UserService userService;

    @Autowired
    PassportQueueService passportQueueService;

    @Autowired
    SnilsQueueService snilsQueueService;  
  
    @Autowired
    PassportService passportService;

    @Autowired
    SnilsService snilsService;
    
    @RequestMapping("/operator/dashboard/index")
    public String userDashBoardMain(ModelMap model) {
        User operator = userService.findByTelephone(SecurityContextHolder.getContext().getAuthentication().getName());

        model.addAttribute("passportsSize", passportQueueService.getCountPassportsInQueue(operator)/2);
        model.addAttribute("snilsesSize", snilsQueueService.getCountSnilsesInQueue(operator)/2);

        model.addAttribute("active", "dashboard");
        return "operator/dashboard/index";
    }

    @RequestMapping("/operator/dashboard/passport")
    public String userDashBoardPassport(ModelMap model) {
        User operator = userService.findByTelephone(SecurityContextHolder.getContext().getAuthentication().getName());
        Passport passport = passportQueueService.getFirstPassport(operator);

        if (passport != null) {
            Map<String, String> statuses = new LinkedHashMap<String, String>();
            statuses.put(StatusConstantClass.ACTIVE, StatusConstantClass.ACTIVE);
            statuses.put(StatusConstantClass.BLOCKED, StatusConstantClass.BLOCKED);
            model.addAttribute("statuses", statuses);
            model.addAttribute("passport", passport);
            model.put("image", ImageViewTransformer.out(passport.getPath_to_original()));
        } else {
            model.addAttribute("passport", new Passport());
            model.addAttribute("error", "All passports is processed");
        }

        model.addAttribute("active", "dashboard");
        return "operator/dashboard/passport";
    }

    @PostMapping("/operator/dashboard/passport/update")
    public String userDashBoardPassportUpdate(@ModelAttribute("passport") Passport formData) {
        User operator = userService.findByTelephone(SecurityContextHolder.getContext().getAuthentication().getName());

        passportService.updatePassport(passportService.getPassportById(formData.getId()),formData);
        passportQueueService.updatePassportQueue(operator, passportService.getPassportById(formData.getId())); //set reloaded passport after update

        return "redirect:/operator/dashboard/passport";
    }

    @RequestMapping("/operator/dashboard/snils")
    public String userDashBoardSnils(ModelMap model) {
        User operator = userService.findByTelephone(SecurityContextHolder.getContext().getAuthentication().getName());
        Snils snils = snilsQueueService.getFirstSnils(operator);
                
        if (snils != null) {
            Map<String, String> statuses = new LinkedHashMap<String, String>();
            statuses.put(StatusConstantClass.ACTIVE, StatusConstantClass.ACTIVE);
            statuses.put(StatusConstantClass.BLOCKED, StatusConstantClass.BLOCKED);
            model.addAttribute("statuses", statuses);
            model.addAttribute("snils", snils);
            model.put("image", ImageViewTransformer.out(snils.getPath_to_original()));
        } else {
            model.addAttribute("snils", new Snils());
            model.addAttribute("error", "All snilses is processed");
        }

        model.addAttribute("active", "dashboard");
        return "operator/dashboard/snils";
    }

    @PostMapping("/operator/dashboard/snils/update")
    public String userDashBoardSnilsUpdate(@ModelAttribute("snils") Snils formData) {
        User operator = userService.findByTelephone(SecurityContextHolder.getContext().getAuthentication().getName());

        snilsService.updateSnils(snilsService.getSnilsById(formData.getId()),formData);
        snilsQueueService.updateSnilsQueue(operator, snilsService.getSnilsById(formData.getId())); //set reloaded passport after update


        return "redirect:/operator/dashboard/snils";
    }

    //Fix error "400 - BadRequest" - convert any date to String 
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }
}
