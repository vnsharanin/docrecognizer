package com.shar.controller.visitor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class VisitorDashboardController {

    @RequestMapping(value = {"/", "/home", "/visitor/home", "/visitor/dashboard"}, method = RequestMethod.GET)
    public String homePage(ModelMap model) {
        model.addAttribute("active", "home");   
        return "index";
    }
    
}
