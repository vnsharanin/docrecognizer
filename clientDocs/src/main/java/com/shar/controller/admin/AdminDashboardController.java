package com.shar.controller.admin;

import com.shar.domain.OperatorCoefficient;
import com.shar.domain.User;
import com.shar.service.PassportQueueService;
import com.shar.service.PassportService;
import com.shar.service.SnilsQueueService;
import com.shar.service.SnilsService;
import com.shar.service.UserService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AdminDashboardController {

    @Autowired
    UserService userService;

    @Autowired
    PassportQueueService passportQueueService;

    @Autowired
    SnilsQueueService snilsQueueService;

     @Autowired
    PassportService passportService;

    @Autowired
    SnilsService snilsService;   
    
    @RequestMapping("/admin/dashboard/index")
    public String userDashBoardMain(ModelMap model) {
        // User user = userService.findByTelephone(SecurityContextHolder.getContext().getAuthentication().getName());
        List<User> operators = userService.listActiveOperators();

        int countPassportsInQueueByDate = passportQueueService.countPassportsInQueueByDate(new Date());
        model.addAttribute("operatorsAndPassports", countPassportsInQueueByDate);
        model.addAttribute("systemAndPassports", passportService.countPassportByDate(new Date()) - countPassportsInQueueByDate);

        int countSnilsesInQueueByDate = snilsQueueService.countSnilsesInQueueByDate(new Date());
        model.addAttribute("operatorsAndSnilses", countSnilsesInQueueByDate);
        model.addAttribute("systemAndSnilses", snilsService.countSnilsByDate(new Date()) - countSnilsesInQueueByDate);

        model.addAttribute("active", "dashboard");
        return "admin/dashboard/index";
    }

    @ResponseBody
    @RequestMapping("/admin/dashboard/index/OnDate")
    public String userDashBoardMainOnDate(@RequestParam("date") java.util.Date date) {
        // User user = userService.findByTelephone(SecurityContextHolder.getContext().getAuthentication().getName());

        System.out.println(date);
        Map<String, Integer> coefficients = new HashMap<String, Integer>();

        int countPassportsInQueueByDate = passportQueueService.countPassportsInQueueByDate(date);
        coefficients.put("operatorsAndPassports", countPassportsInQueueByDate);
        coefficients.put("systemAndPassports", passportService.countPassportByDate(date) - countPassportsInQueueByDate);

        int countSnilsesInQueueByDate = snilsQueueService.countSnilsesInQueueByDate(date);
        coefficients.put("operatorsAndSnilses", countSnilsesInQueueByDate);
        coefficients.put("systemAndSnilses", snilsService.countSnilsByDate(date) - countSnilsesInQueueByDate);

        
        return toJson(coefficients);
    }

    @RequestMapping("/admin/dashboard/statistics")
    public String userDashBoardStatistics(ModelMap model) {
        // User user = userService.findByTelephone(SecurityContextHolder.getContext().getAuthentication().getName());
        List<User> operators = userService.listActiveOperators();

        Map<String, Double> coefficientsProcessedPassports = passportQueueService.calcCoefficient(operators);
        Map<String, Double> coefficientsProcessedSnilses = snilsQueueService.calcCoefficient(operators);
        List<OperatorCoefficient> coefficients = new ArrayList<OperatorCoefficient>(operators.size());
        for (User operator : operators) {
            double cps = coefficientsProcessedSnilses.get(operator.getTelephone());
            double cpp = coefficientsProcessedPassports.get(operator.getTelephone());
            coefficients.add(new OperatorCoefficient(operator.getTelephone(), cpp, cps));
        }
        model.addAttribute("coefficients", coefficients);

        model.addAttribute("active", "dashboard");
        return "admin/dashboard/statistics";
    }

    private String toJson(Object value) {
        ObjectMapper mapper = new ObjectMapper();
        String result = null;
        try {
            result = mapper.writeValueAsString(value);
        } catch (IOException ex) {
            Logger.getLogger(AdminDashboardController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}
