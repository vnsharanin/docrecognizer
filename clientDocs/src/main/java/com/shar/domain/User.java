package com.shar.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "Users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "telephone", unique = true, nullable = false)
    private String telephone;

    @NotNull
    //@Size(min = 4)
    @Column(name = "PASSWORD", nullable = false)
    private String password;

    //для пользователя и оператор: Active,Blocked (в отпуске или реально заблокирован)
    @Column(name = "STATUS", nullable = false)
    private String status;// = Status.ACTIVE.getStatus();

    //для системы безопасности
    @Cascade({CascadeType.ALL})
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    private Set<UserRole> userRoles = new HashSet<UserRole>();

    //для пользователя
    @Cascade({CascadeType.ALL})
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    private Set<Passport> userPassports = new HashSet<Passport>();

    //для пользователя
    @Cascade({CascadeType.ALL})
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    private Set<Snils> userSnilses = new HashSet<Snils>();

    //для оператора
    @Cascade({CascadeType.ALL})
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    private Set<SnilsQueue> userSnilsesInQueue = new HashSet<SnilsQueue>();

    //для оператора
    @Cascade({CascadeType.ALL})
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    private Set<PassportQueue> userPassportsInQueue = new HashSet<PassportQueue>();

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    /*
     public Set<Role> getRole() {
     return role;
     }

     public void setRole(Set<Role> role) {
     this.role = role;
     }*/

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                //.append(email)
                .append(telephone)
                //.append(password)
                .append(status)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof User)) {
            return false;
        }
        User other = (User) obj;
        if (id != other.id) {
            return false;
        }
        if (telephone == null) {
            if (other.telephone != null) {
                return false;
            }
        } else if (!telephone.equals(other.telephone)) {
            return false;
        }

        if (password == null) {
            if (other.password != null) {
                return false;
            }
        } else if (!password.equals(other.password)) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "User [id=" + id + ", ssoId=" + telephone + ", password=" + password
                + ", email=" + telephone + ", state=" + status + "]";
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Set<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(Set<UserRole> userRoles) {
        this.userRoles = userRoles;
    }

    public User() {
    }

    public User(String telephone, String password) {
        this.telephone = telephone;
        this.password = password;
    }

    public Set<Passport> getUserPassports() {
        return userPassports;
    }

    public void setUserPassports(Set<Passport> userPassports) {
        this.userPassports = userPassports;
    }

    public Set<Snils> getUserSnilses() {
        return userSnilses;
    }

    public void setUserSnilses(Set<Snils> userSnilses) {
        this.userSnilses = userSnilses;
    }

    public Set<SnilsQueue> getUserSnilsesInQueue() {
        return userSnilsesInQueue;
    }

    public void setUserSnilsesInQueue(Set<SnilsQueue> userSnilsesInQueue) {
        this.userSnilsesInQueue = userSnilsesInQueue;
    }

    public Set<PassportQueue> getUserPassportsInQueue() {
        return userPassportsInQueue;
    }

    public void setUserPassportsInQueue(Set<PassportQueue> userPassportsInQueue) {
        this.userPassportsInQueue = userPassportsInQueue;
    }
}
