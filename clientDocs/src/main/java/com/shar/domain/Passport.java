package com.shar.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "Passports")
public class Passport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne//(fetch = FetchType.LAZY)
    @JoinColumn(name = "user")//, nullable = false)
    private User user;

    @Column(name = "seria", nullable = true)
    private String seria;

    @Column(name = "number", nullable = true)
    private String number;

    @Column(name = "department", nullable = true)
    private String department;

    @Column(name = "date_of_issue", nullable = true)
    private String dateOfIssue;

    @Column(name = "code", nullable = true)
    private String code;

    @Column(name = "secondname", nullable = true)
    private String secondname;

    @Column(name = "firstname", nullable = true)
    private String firstname;

    @Column(name = "thirdname", nullable = true)
    private String thirdname;

    @Column(name = "sex", nullable = true)
    private String sex;

    @Column(name = "birthdate", nullable = true)
    private String birthdate;

    @Column(name = "birthplace", nullable = true)
    private String birthplace;

    @Column(name = "identify_string", nullable = true)
    private String identify_string;

    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "path_to_original", nullable = false)
    private String path_to_original;
    
    @Column(name = "load_date", nullable = true)
    private Date loadDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getSeria() {
        return seria;
    }

    public void setSeria(String seria) {
        this.seria = seria;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDateOfIssue(String dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSecondname() {
        return secondname;
    }

    public void setSecondname(String secondname) {
        this.secondname = secondname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getThirdname() {
        return thirdname;
    }

    public void setThirdname(String thirdname) {
        this.thirdname = thirdname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }

    public String getIdentify_string() {
        return identify_string;
    }

    public void setIdentify_string(String identify_string) {
        this.identify_string = identify_string;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPath_to_original() {
        return path_to_original;
    }

    public void setPath_to_original(String path_to_original) {
        this.path_to_original = path_to_original;
    }
    
        @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append(id)
                .append(user.getTelephone())
                .append(seria)
                .append(number)
                .append(department)
                .append(dateOfIssue)
                .append(code)
                .append(secondname)
                .append(firstname)
                .append(thirdname)
                .append(sex)
                .append(birthdate)
                .append(birthplace)
                .append(identify_string)
                .append(path_to_original)
                .append(status)
                .toString();
    }

    public Date getLoadDate() {
        return loadDate;
    }

    public void setLoadDate(Date loadDate) {
        this.loadDate = loadDate;
    }
}
