package com.shar.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "Snilses")
public class Snils {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne//(fetch = FetchType.LAZY)
    @JoinColumn(name = "user")//, nullable = false)
    private User user;

    @Column(name = "number", nullable = true)
    private String number;

    @Column(name = "secondname", nullable = true)
    private String secondname;

    @Column(name = "firstname", nullable = true)
    private String firstname;

    @Column(name = "thirdname", nullable = true)
    private String thirdname;
    @Column(name = "birthdate", nullable = true)
    private String birthdate;

    @Column(name = "birthplace", nullable = true)
    private String birthplace;
    @Column(name = "sex", nullable = true)
    private String sex;

    @Column(name = "registerdate", nullable = true)
    private String registerdate;

    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "path_to_original", nullable = false)
    private String path_to_original;
    
    @Column(name = "load_date", nullable = true)
    private Date loadDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getSecondname() {
        return secondname;
    }

    public void setSecondname(String secondname) {
        this.secondname = secondname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getThirdname() {
        return thirdname;
    }

    public void setThirdname(String thirdname) {
        this.thirdname = thirdname;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getRegisterdate() {
        return registerdate;
    }

    public void setRegisterdate(String registerdate) {
        this.registerdate = registerdate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPath_to_original() {
        return path_to_original;
    }

    public void setPath_to_original(String path_to_original) {
        this.path_to_original = path_to_original;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append(id)
                .append(user.getTelephone())
                .append(number)
                .append(secondname)
                .append(firstname)
                .append(thirdname)
                .append(birthdate)
                .append(birthplace)
                .append(sex)
                .append(registerdate)
                .append(path_to_original)
                .append(status)
                .toString();
    }

    public Date getLoadDate() {
        return loadDate;
    }

    public void setLoadDate(Date loadDate) {
        this.loadDate = loadDate;
    }
}
