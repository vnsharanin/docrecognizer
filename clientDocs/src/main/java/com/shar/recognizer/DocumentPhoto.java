/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shar.recognizer;

import com.shar.configuration.MyGlobal;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.objdetect.CascadeClassifier;

/**
 *
 * @author v_sh9
 */
public class DocumentPhoto {

    public static boolean isPassport(String srcIn, String fileName) {
        System.load(MyGlobal.CONFIG_FOLDER + "opencv_java249.dll");
        CascadeClassifier faceDetector = new CascadeClassifier(MyGlobal.CONFIG_FOLDER + "haarcascade_frontalface_alt.xml");
        Mat image = Highgui
                .imread(srcIn);

        MatOfRect faceDetections = new MatOfRect();
        faceDetector.detectMultiScale(image, faceDetections);
        System.out.println(String.format("Detected %s faces", faceDetections.toArray().length));

        //—охраним только лицо (если оно найдено):
        if (faceDetections.toArray().length >= 1) {
            Rect rectCrop = null;
            int startPosX = 0;
            int startPosY = 0;
            int endPosX = 0;
            int endPosY = 0;
            for (Rect rect : faceDetections.toArray()) {
                Core.rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height),
                        new Scalar(0, 255, 0));
                startPosX = rect.x;
                startPosY = rect.y;
                endPosX = startPosX + rect.width;
                endPosY = startPosY + rect.height;
                rectCrop = new Rect(startPosX, startPosY, rect.width, rect.height);
                break;//����� ������� � �� �������
            }
            Mat image_roi = new Mat(image, rectCrop);
            Highgui.imwrite(MyGlobal.UPLOADED_FACES_FOLDER + fileName, image_roi);

            int centerImage = image.height() / 2;
            //�������� 25% ������� �����
            //image.width()-startPosX �������� ��� ���������� �� ������
            String topPartPath = MyGlobal.UPLOADED_TEMP_PASSPORTS_FOLDER + "top" + fileName;
            rectCrop = new Rect(startPosX, startPosX, image.width() - startPosX, centerImage / 2);//(image.height()-startPosY)/2);//new Rect(50, 50, startPosX/2, startPosY/2);
            image_roi = new Mat(image, rectCrop);
            Highgui.imwrite(topPartPath, image_roi);
            //����� ����������� � �����������
            converting(topPartPath, topPartPath, -2, 600); //sharain
            // converting(topPartPath,topPartPath,-2,285); //gusev
            //converting(source,topPartPath, 1, 0); //white-black

            //�������� 75% ������ ����� (�� �� ������ ������ ��������� ��� ������ �����)
            //�������� ��� ��� ��������� ����� �� x � � ��������
            //startPosY-(endPosY-startPosY) ��� �� ������� ���� ����� ����� �� ������� ��� ��� ����� �����
            String lowPartPath = MyGlobal.UPLOADED_TEMP_PASSPORTS_FOLDER + "low" + fileName;
            rectCrop = new Rect(endPosX, centerImage, image.width() - endPosX, (int) Math.round(centerImage * 0.95));
            image_roi = new Mat(image, rectCrop);
            Highgui.imwrite(lowPartPath, image_roi);
            converting(lowPartPath, lowPartPath, -5.2, 1000);//my passport
            //converting(source,lowPartPath, 1, 0);//white-black

            return true;
        }
        return false;
    }

    public static void isSnils(String srcIn, String fileName) {
        System.load(MyGlobal.CONFIG_FOLDER + "opencv_java249.dll");
        //-120 11000 sharanin
        //-4 600; -30 4000 
        converting(srcIn, MyGlobal.UPLOADED_TEMP_SNILSES_FOLDER + fileName, -120, 11000);
    }

    public static void converting(String srcIn, String srcOut, double alpha, double beta) {
        Mat source = Highgui.imread(srcIn,
                Highgui.CV_LOAD_IMAGE_GRAYSCALE);
        Mat destination = new Mat(20, 20, source.type());
        source.convertTo(destination, -1, alpha, beta);
        Highgui.imwrite(srcOut, destination);
    }
}
