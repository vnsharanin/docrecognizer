package com.shar.recognizer;

import com.shar.configuration.MyGlobal;
import com.shar.dao.SnilsDaoImpl;
import com.shar.domain.LanguageConstantClass;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import org.apache.log4j.Logger;

import org.bytedeco.javacpp.*;
import static org.bytedeco.javacpp.lept.*;
import static org.bytedeco.javacpp.tesseract.*;

public class DocumentText {

    private static final Logger logger = Logger.getLogger(DocumentText.class);

    public static String[] doOCR(String srcIn, String language) {
        BytePointer outText;

        TessBaseAPI api = new TessBaseAPI();
        // Initialize tesseract-ocr with English, without specifying tessdata path
        if (api.Init(MyGlobal.CONFIG_FOLDER, language) != 0) {
            System.err.println("Could not initialize tesseract.");
            System.exit(1);
        }

        File imgPath = new File(srcIn);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BufferedImage img;

        try {
            img = ImageIO.read(imgPath);
            ImageIO.write(img, "png", baos);
        } catch (IOException e) {
            System.err.println("Reading file or writing byte[] failed.");
            e.printStackTrace();
        }

        byte[] imageInByte = baos.toByteArray();

        PIX image = lept.pixReadMemPng(imageInByte, imageInByte.length);

        api.SetImage(image);
        // Get OCR result
        outText = api.GetUTF8Text();
        String[] docLines = null;
        try {
            String docText = "";
            if (language == LanguageConstantClass.RUS)
                docText = new String(outText.getString().getBytes("windows-1251"), "UTF-8");
            else
                docText = outText.getString();
            logger.debug(docText);
            docLines = docText.split("\r\n|\r|\n");
        } catch (Exception ex) {
        }
        // Destroy used object and release memory
        api.End();
        outText.deallocate();
        pixDestroy(image);
        return docLines;
    }
}
