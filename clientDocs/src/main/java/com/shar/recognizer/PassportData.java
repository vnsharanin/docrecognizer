package com.shar.recognizer;

import com.shar.configuration.MyGlobal;
import com.shar.domain.LanguageConstantClass;
import com.shar.domain.Passport;
import com.shar.domain.StatusConstantClass;
import org.apache.log4j.Logger;

public class PassportData {

    private static final Logger logger = Logger.getLogger(PassportData.class);

    public static void fillPassportData(Passport passport, String fileName) {
        if (DocumentPhoto.isPassport(passport.getPath_to_original(), fileName)) {
            String[] docTextTop = DocumentText.doOCR(MyGlobal.UPLOADED_TEMP_PASSPORTS_FOLDER + "top" + fileName, LanguageConstantClass.RUS);//call from administrator!!!
            //logger.debug("PASSPORT>OCR top:\n" + Arrays.toString(docTextTop));
            String[] docTextLow = DocumentText.doOCR(MyGlobal.UPLOADED_TEMP_PASSPORTS_FOLDER + "low" + fileName, LanguageConstantClass.RUS);//call from administrator!!!
            //logger.debug("PASSPORT>OCR low:\n" + Arrays.toString(docTextLow));

            //check. If all data filled then saccess and exit else review
            passport.setStatus(StatusConstantClass.REVIEW);//при этом мы не будем довер§ть качеству распознавани§ и зафейлим, чтобы оператор правил
        } else {
            passport.setStatus(StatusConstantClass.BLOCKED);//blocked on system level
        }
    }
}
