package com.shar.recognizer;

import com.shar.configuration.MyGlobal;
import com.shar.domain.LanguageConstantClass;
import com.shar.domain.Snils;
import com.shar.domain.StatusConstantClass;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;

public class SnilsData {

    private static final Logger logger = Logger.getLogger(SnilsData.class);

    public static void fillSnilsData(Snils snils, String fileName) {
        //Prepare images with openCV for OCR
        DocumentPhoto.isSnils(snils.getPath_to_original(), fileName);

        //Recognizing text
        String[] docLines = DocumentText.doOCR(MyGlobal.UPLOADED_TEMP_SNILSES_FOLDER + fileName, LanguageConstantClass.RUS);//call from administrator!!!
        //logger.debug("SNILS>OCR output:\n" + Arrays.toString(docText));

        //Processing recognized text
        String nextField = "Number";
        for (String docLine : docLines) {
            String docLineTrasformed = "";
            for (char symbolWin1251 : docLine.replaceAll("[?]", "И").toUpperCase().trim().toCharArray()) {
                String symbolUTF8 = "";
                try {
                    symbolUTF8 = new String(String.valueOf(symbolWin1251).getBytes("UTF-8"), "windows-1251");
                } catch (UnsupportedEncodingException ex) {
                    logger.info(ex.getMessage());
                }
                if ((int) symbolUTF8.charAt(0) != 1087 && (int) symbolWin1251 != 65533) { //unknown symbol '?'
                    docLineTrasformed += symbolWin1251;
                }
            }
            //logger.debug(docLineTrasformed);

            //NUMBER
            if (Pattern.matches("^[0-9]{3}-[0-9]{3}-[0-9]{3}[- ][0-9]{2}$", docLineTrasformed)) {
                snils.setNumber(docLineTrasformed);
                logger.debug(docLineTrasformed);
                nextField = "FIO";
                continue;
            }
            //FIO
            //if (Pattern.matches("^[А-Я]{1}[. ][А-Я]{1}[. ][А-Я]{1}[. ]$", docLineTrasformed)) {
            if (nextField == "FIO") {
                if (snils.getSecondname() == null) {
                    snils.setSecondname(docLineTrasformed.replaceFirst("[А-Я]{1}[. ][А-Я]{1}[. ][А-Я]{1}[. ]", "").trim());
                    logger.debug(docLineTrasformed);
                    continue;
                }
                if (snils.getFirstname() == null) {
                    snils.setFirstname(docLineTrasformed.replaceFirst("[А-Я]{1}[. ][А-Я]{1}[. ][А-Я]{1}[. ]", "").trim());
                    logger.debug(docLineTrasformed);
                    continue;
                }
                if (snils.getThirdname() == null) {
                    snils.setThirdname(docLineTrasformed.replaceFirst("[А-Я]{1}[. ][А-Я]{1}[. ][А-Я]{1}[. ]", "").trim());
                    nextField = "BirthAndPlace";
                    logger.debug(docLineTrasformed);
                    continue;
                }
            }
            if (nextField == "BirthAndPlace") {
                //if (snils.getSex() == null) {
                if (docLineTrasformed.contains("ПОЛ") && (docLineTrasformed.contains("ЖЕН") || docLineTrasformed.contains("МУЖ"))) {
                    snils.setSex(docLineTrasformed.replaceFirst("ПОЛ", "").trim());
                    nextField = "RegisterDate";
                    logger.debug(docLineTrasformed);
                    continue;
                }
                if (snils.getBirthdate() == null) {
                    snils.setBirthdate(docLineTrasformed.replaceFirst("ДАТА И МЕСТО РОЖДЕНИЯ", "").trim());
                    logger.debug(docLineTrasformed);
                    continue;
                }
                if (snils.getBirthdate() != null) {//getBirthdate - it's ok
                    String birthplace = "";
                    birthplace += (snils.getBirthplace() != null) ? snils.getBirthplace() + " " : "";
                    birthplace += docLineTrasformed.replaceFirst("ДАТА И МЕСТО РОЖДЕНИЯ", "").trim();
                    snils.setBirthplace(birthplace);
                    logger.debug(docLineTrasformed);
                    continue;
                }

            }
            /*if (nextField == "Sex") {

             }*/
            if (nextField == "RegisterDate") {
                snils.setRegisterdate(docLineTrasformed.replaceFirst("ДАТА РЕГИСТРАЦИИ", "").trim());
                logger.debug(docLineTrasformed);
                continue;
            }
        }

        //check. If all data filled then saccess and exit else fail and set to queue
        if (snils.getNumber() != null
                && snils.getSecondname() != null
                && snils.getFirstname() != null
                && snils.getThirdname() != null
                && snils.getBirthdate() != null
                && snils.getBirthplace() != null
                && snils.getSex() != null
                && snils.getRegisterdate() != null) {
            snils.setStatus(StatusConstantClass.ACTIVE);
        } else {
            snils.setStatus(StatusConstantClass.REVIEW); //operator will get it
        }
    }
}
