package com.shar.configuration;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MyGlobal {
    //opencv_java249.dll,haarcascade_frontalface_alt.xml
    public static String CONFIG_FOLDER = "C://ServerFiles//config//";
    public static String UPLOADED_TEMP_PASSPORTS_FOLDER = "C://ServerFiles//temp//passports//";
    public static String UPLOADED_TEMP_SNILSES_FOLDER = "C://ServerFiles//temp//snilses//";
    public static String UPLOADED_PASSPORTS_FOLDER = "C://ServerFiles//passports//";
    public static String UPLOADED_SNILSES_FOLDER = "C://ServerFiles//snilses//";
    public static String UPLOADED_FACES_FOLDER = "C://ServerFiles//faces//";
    public static int MAX_FILE_SIZE = 20*100000;
            
    public static String getCurrentDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yy HH:mm");
        java.util.Date currentDate = new java.util.Date();
        return sdf.format(currentDate);
        /*String date = sdf.format(currentDate); 
        return (Date) sdf.parse(date);*/
    }
}
