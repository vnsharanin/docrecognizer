/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shar.dao;

import com.shar.domain.PassportQueue;
import java.util.List;

/**
 *
 * @author v_sh9
 */
public interface PassportQueueDao {
     void createOrUpdatePassportQueue(PassportQueue passportQueue);
    List<PassportQueue> getListPassportQueue();
}
