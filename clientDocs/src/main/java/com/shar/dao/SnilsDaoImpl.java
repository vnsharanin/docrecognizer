package com.shar.dao;

import com.shar.configuration.MyGlobal;
import com.shar.domain.Snils;
import com.shar.domain.SnilsQueue;
import com.shar.domain.StatusConstantClass;
import com.shar.domain.User;
import com.shar.recognizer.SnilsData;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import org.springframework.stereotype.Repository;

import java.util.List;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

@Repository("SnilsDao")
public class SnilsDaoImpl extends AbstractDao<Integer, Snils> implements SnilsDao {

    private static final Logger logger = Logger.getLogger(SnilsDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;
    Transaction tx = null;

    public Snils saveSnils(byte[] bytes, User user) {
        String fileName = user.getTelephone() + ".png";
        String fullPath = MyGlobal.UPLOADED_SNILSES_FOLDER + fileName;
        try {
            //saving original snils image
            BufferedImage resultImage = ImageIO.read(new ByteArrayInputStream(bytes));
            ImageIO.write(resultImage, "png", new File(MyGlobal.UPLOADED_SNILSES_FOLDER, fileName));//фотку сохранили

             //saving snils data
            Snils snils = new Snils();
            snils.setPath_to_original(fullPath);
            snils.setUser(user);
            snils.setLoadDate(new java.util.Date());
            SnilsData.fillSnilsData(snils, fileName);
            sessionFactory.getCurrentSession().save(snils);
            if (snils.getStatus() == StatusConstantClass.REVIEW) {
                //ищем активного оператора (чтоб не кидало на тех кто в отпуске)
                Query query = sessionFactory.getCurrentSession().createQuery(""
                        + "select u from User u"
                        + " inner join u.userRoles ur"
                        + " inner join ur.role r"
                        + " left outer join u.userSnilsesInQueue sq"
                        + " where r.name = :role and u.status = :status group by u.id order by count(sq.user)");
                query.setString("status", StatusConstantClass.ACTIVE);
                query.setString("role", "OPERATOR");
                query.setFirstResult(0);
                query.setMaxResults(1);
                User operator = (User) query.uniqueResult();
                // operator.getUserPassportsInQueue().add(new PassportQueue());
                SnilsQueue snilsQueue = new SnilsQueue();
                snilsQueue.setSnils(snils);
                snilsQueue.setUser(operator);
                snilsQueue.setStatus(StatusConstantClass.REVIEW);
                snilsQueue.setStartDate(new java.util.Date());
                sessionFactory.getCurrentSession().save(snilsQueue);
            }
            return snils;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public List<Snils> listSnilses() {
        return sessionFactory.getCurrentSession().createQuery("from Snils").list();
    }
    public void updateSnils(Snils snils) {
        sessionFactory.getCurrentSession().update(snils);
    }

    public Snils getSnilsById(int id) {
        Query query = sessionFactory.getCurrentSession().createQuery(""
                + "select s from Snils s"
                + " where s.id = :id");
        query.setInteger("id", id);
        query.setFirstResult(0);
        query.setMaxResults(1);
        return (Snils) query.uniqueResult();
    }
}
