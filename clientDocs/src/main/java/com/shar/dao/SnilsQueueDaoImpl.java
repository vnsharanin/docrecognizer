/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shar.dao;

import com.shar.domain.SnilsQueue;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("SnilsQueueDao")
public class SnilsQueueDaoImpl implements  SnilsQueueDao{

    @Autowired
    private SessionFactory sessionFactory;
    
    public void createOrUpdateSnilsQueue(SnilsQueue snilsQueue) {
        if (snilsQueue.getId() > 0) {
            sessionFactory.getCurrentSession().update(snilsQueue);
        } else {
            sessionFactory.getCurrentSession().save(snilsQueue);
        }
    }
     public List<SnilsQueue> getListSnilsQueue(){
         return sessionFactory.getCurrentSession().createQuery("from SnilsQueue").list();
    }
}
