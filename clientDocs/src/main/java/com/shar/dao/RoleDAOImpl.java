package com.shar.dao;

import com.shar.domain.Role;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("RoleDAO")
public class RoleDAOImpl implements RoleDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public List<Role> listRoles() {
        return sessionFactory.getCurrentSession().createQuery("from Role").list();
    }

    public Role findById(int id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Role where id = :id");
        query.setInteger("id", id);
        return (Role) query.uniqueResult();
    }
    
    public Role findByName(String role) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Role where name = :role");
        query.setString("role", role);
        return (Role) query.uniqueResult();
    }
    
    public void saveRole(Role role) {
        sessionFactory.getCurrentSession().saveOrUpdate(role);
    }
}
