package com.shar.service;

import com.shar.domain.Passport;
import com.shar.domain.User;
import java.util.Date;
import java.util.List;

public interface PassportService {
        List<Passport> listPassports();
	Passport savePassport(byte[] bytes, User user);
        Passport getLastPassport(User user);
        void updatePassport(Passport passport,Passport formData);
        Passport getPassportById(int id);
        public int countPassportByDate(Date date);
}