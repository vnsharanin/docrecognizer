package com.shar.service;

import com.shar.domain.User;
import java.util.List;

public interface UserService {
        List<User> listUsers();
        List<User> listActiveOperators();
	User findById(int id);
	User saveUser(User user);
        User findByTelephone(String telephone);
}