/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shar.service;

import com.shar.dao.SnilsQueueDao;
import com.shar.domain.SnilsQueue;
import com.shar.domain.Snils;
import com.shar.domain.StatusConstantClass;
import com.shar.domain.User;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("snilsQueueService")
@Transactional
public class SnilsQueueServiceImpl implements SnilsQueueService {

    @Autowired
    private SnilsQueueDao dao;

    private static final Logger logger = Logger.getLogger(SnilsQueueServiceImpl.class);

    
    
    public Snils getFirstSnils(User operator) {
        int size = operator.getUserSnilsesInQueue().size();
        if (size != 0) {
            SnilsQueue[] snilsesInQueue = operator.getUserSnilsesInQueue().toArray(new SnilsQueue[size]);
            Arrays.sort(snilsesInQueue, SnilsQueue.StartDateComparator);
            for (SnilsQueue sq : snilsesInQueue) {
                if (sq.getStatus().equals(StatusConstantClass.REVIEW)) {
                    return sq.getSnils();
                }
            }
        }
        return null;
    }

    public int getCountSnilsesInQueue(User operator) {
        int size = operator.getUserSnilsesInQueue().size();
        if (size != 0) {
            SnilsQueue[] snilsesInQueue = operator.getUserSnilsesInQueue().toArray(new SnilsQueue[size]);
            Arrays.sort(snilsesInQueue, SnilsQueue.StartDateComparator);
            for (SnilsQueue sq : snilsesInQueue) {
                if (sq.getStatus().equals(StatusConstantClass.REVIEW)) {
                    size++;
                }
            }
        }
        return size;
    }

    public void updateSnilsQueue(User operator, Snils snils) {
        SnilsQueue snilsQueue = null;
        for (SnilsQueue sq : operator.getUserSnilsesInQueue()) {
            if (sq.getUser().getId() == operator.getId() && sq.getSnils().getId() == snils.getId()) {
                snilsQueue = sq;
            }
        }
        if (snilsQueue == null) {
            snilsQueue.setUser(operator);
            snilsQueue.setSnils(snils);
            snilsQueue.setStartDate(new java.util.Date());
        }
        snilsQueue.setEndDate(new java.util.Date());
        snilsQueue.setStatus(StatusConstantClass.SUCCESS);
        dao.createOrUpdateSnilsQueue(snilsQueue);
    }

    public Map<String, Double> calcCoefficient(List<User> operators) {
        List<SnilsQueue> snilsesQueue = dao.getListSnilsQueue();
        Map<String, Double> result = new HashMap<String, Double>(operators.size());

        for (User operator : operators) {
            int countSnilses = 0;
            int countSnilsesInTime = 0;
            for (SnilsQueue sq : snilsesQueue) {
                if (sq.getUser().getId() == operator.getId()) {
                    if (sq.getStartDate().getMonth() == new Date().getMonth()) {
                        countSnilses++;
                        if (sq.getStartDate().equals(sq.getEndDate())) {
                            countSnilsesInTime++;
                        }
                    }
                }
            }
            double coefficient = countSnilses - countSnilsesInTime;//(countSnilsesInTime != 0) ? countSnilses / countSnilsesInTime : countSnilses;
            result.put(operator.getTelephone(), coefficient);
        }
        return result;
    }

    public int countSnilsesInQueueByDate(Date date) {
        int i = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for (SnilsQueue sq : dao.getListSnilsQueue()) {
            if (sdf.format(sq.getStartDate()).equals(sdf.format(date))) {
                i++;
            }
        }
        logger.debug("countSnilsesInQueueByDate="+i+" Date:"+sdf.format(date));
        return i;
    }
}
