package com.shar.service;

import com.shar.dao.PassportDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.shar.domain.Passport;
import com.shar.domain.StatusConstantClass;
import com.shar.domain.User;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

@Service("passportService")
@Transactional
public class PassportServiceImpl implements PassportService {

    private static final Logger logger = Logger.getLogger(PassportServiceImpl.class);
    @Autowired
    private PassportDao dao;

    public List<Passport> listPassports() {
        return dao.listPassports();
    }

    public Passport savePassport(byte[] bytes, User user) {
        return dao.savePassport(bytes, user);
    }

    public Passport getLastPassport(User user) {
        //we can get it from user object, that's why we won't use DAO
        //one of statuses must be
        /*for (Passport passport : user.getUserPassports()) {
            if (passport.getStatus().equals(StatusConstantClass.REVIEW)) {
                logger.debug(passport.toString());
                return passport;
            }
        }
        for (Passport passport : user.getUserPassports()) {
            if (passport.getStatus().equals(StatusConstantClass.ACTIVE)) {
                logger.debug(passport.toString());
                return passport;
            }
        }
        for (Passport passport : user.getUserPassports()) {
            if (passport.getStatus().equals(StatusConstantClass.BLOCKED)) {
                logger.debug(passport.toString());
                return passport;
            }
        }*/
        int pos = 1;
        for (Passport passport : user.getUserPassports()) {
            if ((user.getUserPassports().size()) == pos) {
                logger.debug(passport.toString());
                return passport;
            }
            pos++;
        }
        logger.debug("Passport not found");
        return null;
    }

    public Passport getPassportById(int id) {
        return dao.getPassportById(id);
    }

    public void updatePassport(Passport passport, Passport formData) {
        //previous passports to fail
        for (Passport previousPassport : passport.getUser().getUserPassports()) {
            if (previousPassport.getId() != passport.getId()) {
                previousPassport.setStatus(StatusConstantClass.BLOCKED);
                dao.updatePassport(previousPassport);
            }
        }

        passport.setBirthdate(formData.getBirthdate());
        passport.setBirthplace(formData.getBirthplace());
        passport.setCode(formData.getCode());
        passport.setDateOfIssue(formData.getDateOfIssue());
        passport.setDepartment(formData.getDepartment());
        passport.setFirstname(formData.getFirstname());
        passport.setIdentify_string(formData.getIdentify_string());
        passport.setNumber(formData.getNumber());
        passport.setSecondname(formData.getSecondname());
        passport.setSeria(formData.getSeria());
        passport.setSex(formData.getSex());
        passport.setStatus(formData.getStatus());
        passport.setThirdname(formData.getThirdname());
        dao.updatePassport(passport);
    }

    public int countPassportByDate(Date date) {
        int i = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for (Passport p : dao.listPassports()) {
            if (sdf.format(p.getLoadDate()).equals(sdf.format(date))) {
                i++;
            }
        }
        logger.debug("countPassportByDate="+i+" Date:"+date);
        return i;
    }

}
