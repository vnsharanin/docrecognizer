package com.shar.service;

import com.shar.domain.Snils;
import com.shar.domain.User;
import java.util.Date;
import java.util.List;

public interface SnilsService {

    List<Snils> listSnilses();

    Snils saveSnils(byte[] bytes, User user);

    Snils getLastSnils(User user);

    void updateSnils(Snils snils, Snils formData);

    Snils getSnilsById(int id);
    
    int countSnilsByDate(Date date);
}
