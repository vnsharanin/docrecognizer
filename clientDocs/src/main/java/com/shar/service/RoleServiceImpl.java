package com.shar.service;

import com.shar.dao.RoleDAO;
import com.shar.domain.Role;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("RoleService")
@Transactional
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDAO roleDAO;

    public List<Role> listRoles() {
        return roleDAO.listRoles();
    }

    public Role findById(int id) {
        return roleDAO.findById(id);
    }
    
    public Role findByName(String role) {
        return roleDAO.findByName(role);
    }
    
    public void saveRole(Role role) {
        roleDAO.saveRole(role);
    }
}
