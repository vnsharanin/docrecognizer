/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shar.service;

import com.shar.domain.Snils;
import com.shar.domain.User;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author v_sh9
 */
public interface SnilsQueueService {
    public Snils getFirstSnils(User operator);
    public void updateSnilsQueue(User operator, Snils snils);
    public int getCountSnilsesInQueue(User operator);
    Map<String,Double> calcCoefficient(List<User> operators);
    int countSnilsesInQueueByDate(Date date);
}
